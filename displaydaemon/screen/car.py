import math
from datetime import datetime
from typing import Optional, List, Dict, Tuple

import isodate

import displaydaemon.d3k as d3k_icons
from displaydaemon.input import KEY_LEFT
from displaydaemon.keys import GPS_DATA, NET_DATA, OBD_DATA, LOG_DATA
from displaydaemon.screen.base import BaseScreen


MIN_GPS_ACC = float(1)
MAX_GPS_ACC = float(150)


GPS_STATE_OFF = 0x00
GPS_STATE_ON = 0x10
GPS_STATE_SEARCHING = 0x11

NET_STATE_OFF = 0x00
NET_STATE_OFFLINE = 0x01
NET_STATE_CONNECTED = 0x11

CAR_STATE_OFF = 0x00
CAR_STATE_OFFLINE = 0x01
CAR_STATE_CONNECTED = 0x11

LOG_STATE_OFF = 0b00
LOG_STATE_ALIVE = 0b01
LOG_STATE_ALIVE_BIT = 0
LOG_STATE_LOGGING = 0b10
LOG_STATE_LOGGING_BIT = 1


class CarScreen(BaseScreen):
    def __init__(self):
        super().__init__(True, True)
        self._gps_state: int = GPS_STATE_OFF
        self._net_state: int = NET_STATE_OFF
        self._car_state: int = CAR_STATE_OFF
        self._eth_ip: Optional[str] = None
        self._usb_ip: Optional[str] = None
        self._wifi_ip: Optional[str] = None
        self._obd_sim_ts: Optional[str] = None
        self._obd_voltage: Optional[float] = None
        self._current_center_line: int = 0
        self._car_soc: Optional[float] = None
        self._car_avail_energy: Optional[float] = None
        self._car_voltage: Optional[float] = None
        self._car_current: Optional[float] = None
        self._car_power: Optional[float] = None
        self._car_speed: Optional[float] = None
        self._car_state: Optional[float] = None
        self._car_eco_mode: Optional[float] = None
        self._car_max_gen_power: Optional[float] = None
        self._car_max_avail_power: Optional[float] = None
        self._temp_battery: Optional[float] = None
        self._temp_cabin: Optional[float] = None
        self._temp_exterior: Optional[float] = None
        self._log_state: int = LOG_STATE_OFF

    def setup_chars(self) -> Dict[int, List[int]]:
        return {
            0: d3k_icons.ICON_INDOOR,
            1: d3k_icons.ICON_OUTDOOR,
            4: d3k_icons.ICON_KILOWATTS,
            5: d3k_icons.ICON_DEG_CELSIUS,
            6: d3k_icons.ICON_BATTERY,
            7: d3k_icons.ICON_TEMPERATURE
        }

    def update_data(self,
                    t: float,
                    data: dict):
        self._gps_state = self._update_gps_state(
            t,
            data.get(GPS_DATA['systimestamp'], None),
            max(data.get(GPS_DATA['epx'], 9999), data.get(GPS_DATA['epy'], 9999)))
        self._net_state = self._update_net_state(
            t,
            data.get(NET_DATA['systimestamp'], None),
            data.get(NET_DATA['expect_next_ts'], None),
            data.get(NET_DATA['is_online'], False))
        self._car_state = self._update_car_state(
            t,
            data.get(OBD_DATA['systimestamp'], None))

        self._obd_sim_ts = data.get(OBD_DATA['systimestamp_sim'], None)

        self._obd_voltage = data.get(OBD_DATA['voltage'], None)
        self._car_soc = data.get(OBD_DATA['bat_soc'], None)
        self._car_avail_energy = data.get(OBD_DATA['bat_available_energy'], None)
        self._car_voltage = data.get(OBD_DATA['bat_voltage'], None)
        self._car_current = data.get(OBD_DATA['bat_current'], None)
        self._car_speed = data.get(OBD_DATA['speed'], None)
        self._car_state = data.get(OBD_DATA['state'], None)
        self._car_eco_mode = data.get(OBD_DATA['eco_mode'], None)
        self._car_max_gen_power = data.get(OBD_DATA['max_charge'], None)
        self._car_max_avail_power = data.get(OBD_DATA['max_power'], None)

        self._temp_battery = data.get(OBD_DATA['temp_bat'], None)
        self._temp_cabin = data.get(OBD_DATA['temp_in'], None)
        self._temp_exterior = data.get(OBD_DATA['temp_out'], None)

        self._car_power = self._car_voltage * self._car_current / -1000 \
            if self._car_voltage is not None and self._car_current is not None \
            else None

        self._log_state = self._update_log_state(t,
                                                 data.get(LOG_DATA['systimestamp']),
                                                 data.get(LOG_DATA['is_logging']))

        self._current_center_line = 0

    def _update_log_state(self,
                          t: float,
                          sys_ts: Optional[str],
                          is_logging: Optional[bool]) -> int:
        if sys_ts is None:
            return LOG_STATE_OFF
        try:
            ts = isodate.parse_datetime(sys_ts).timestamp()
        except:
            return LOG_STATE_OFF

        if t - ts > 8:  # Heartbeat of DataLoggerD is 5s so this should catch any failure
            return LOG_STATE_OFF

        state = LOG_STATE_ALIVE
        if is_logging:
            state = state | LOG_STATE_LOGGING
        return state

    def _update_gps_state(self,
                          t: float,
                          sys_ts: Optional[str],
                          gps_acc: float) -> int:
        if sys_ts is None:
            return GPS_STATE_OFF
        ts = datetime.strptime(sys_ts, '%Y-%m-%d %H:%M:%S.%f').timestamp()
        if t - ts < 10:
            return GPS_STATE_ON \
                if MIN_GPS_ACC < gps_acc < MAX_GPS_ACC else \
                GPS_STATE_SEARCHING
        return GPS_STATE_OFF

    def _update_net_state(self,
                          t: float,
                          sys_ts: Optional[str],
                          expect_next_ts: Optional[str],
                          is_online: bool) -> int:
        if sys_ts is None:
            return NET_STATE_OFF

        try:
            ts = isodate.parse_datetime(sys_ts).timestamp()
            expect_next = isodate.parse_datetime(expect_next_ts).timestamp() \
                if expect_next_ts is not None \
                else 0
        except:
            return NET_STATE_OFF

        if t - ts > 10:
            return NET_STATE_OFF
        if t < expect_next:
            return NET_STATE_CONNECTED if is_online else NET_STATE_OFFLINE
        else:
            return NET_STATE_OFFLINE

    def _update_car_state(self,
                          t: float,
                          sys_ts: Optional[str]) -> int:
        if not sys_ts:
            return CAR_STATE_OFF

        try:
            ts = isodate.parse_datetime(sys_ts).timestamp()
        except:
            return NET_STATE_OFF

        return CAR_STATE_OFFLINE if t > ts + 5 else CAR_STATE_CONNECTED

    def handle_input(self, input: int) -> Optional[str]:
        if input == KEY_LEFT:
            return 'clock'
        return None

    @property
    def is_car_charging(self) -> bool:
        return self._car_power is not None \
               and self._car_power < 0 \
               and not self.is_car_running

    @property
    def is_car_running(self) -> bool:
        return self._car_state == 'VS_ENGINE_RUNNING'

    @property
    def is_car_sleeping(self) -> bool:
        return self._car_state == 'VS_SLEEPING'

    def render(self, t: float) -> List[str]:
        return [
            self._render_line1(t),
            self._render_line2(t),
            self._render_line3(t)
        ]

    def render_backlight(self, t: float, use_dothat: bool) -> Optional[List[Tuple[int, int, int]]]:
        if self.is_car_charging:
            return self._render_charging_animation(t, use_dothat,
                                                   30 if self._car_state == 'VS_SLEEPING' else 100)
        if self.is_car_sleeping:
            return [(0, 0, 0)]

        if self._car_eco_mode is not None:
            color = (0, 100, 0) if self._car_eco_mode == 1 else (200, 0, 0)
        else:
            color = (200, 0, 0)

        return [color]

    def _render_charging_animation(self,
                                   t: float,
                                   use_dothat: bool,
                                   offset: int = 30,
                                   max_value: int = 100) -> List[Tuple[int, int, int]]:
        l = 6 if use_dothat else 3
        o = (t / 5)
        idx = math.floor(o) % l
        color = math.floor(abs(math.sin((o % 1) * math.pi)) * max_value)

        c = []
        for i in range(l):
            c.append((0, 0, color + offset)
                     if (i % 3 == idx % 3)
                     else (0, 0, offset))

        return c

    def _render_top_line(self, t: float) -> str:
        speed = '---'
        if self._car_speed is not None:
            speed = '{:.0f}'.format(self._car_speed)

        sim_ts = ''
        if self._obd_sim_ts:
            try:
                sim_ts = isodate.parse_datetime(self._obd_sim_ts).strftime(' %H:%M:%S')
            except:
                pass

        return '{:>3} kmh{}'.format(speed, sim_ts).center(16)

    def _render_center_line(self, t: float) -> str:
        soc = '---.--'
        if self._car_soc:
            soc = '{:.1f}'.format(self._car_soc)

        cons = '---.-'
        if self._car_voltage and self._car_current:
            cons = '{:.1f}'.format(self._car_power)

        return '{:>6}%  {:>5}{}'.format(soc, cons, chr(4))

    def _build_status_line(self, t: float) -> str:
        return (self._car_state[3:] if self._car_state and self._car_state != "None" else '-').center(16)[0:16]

    def _render_line1(self, t: float) -> str:
        return '{}{:>5}{}  {:>5}{}{}'.format(
            # Power
            ' ',
            '--.-' if self._car_power is None else '{:.1f}'.format(self._car_power),
            chr(4),

            # Temp Bat
            '---.-' if self._temp_battery is None else '{:.1f}'.format(self._temp_battery),
            chr(5),
            chr(6),
        )

    def _render_line2(self, t: float) -> str:
        return '{}{:>5}{}h {:>5}{}{}'.format(
            # Energy
            ' ',
            '--.--' if self._car_avail_energy is None else '{:.2f}'.format(self._car_avail_energy),
            chr(4),

            # Temp Indoor
            '---.-' if self._temp_cabin is None else '{:.1f}'.format(self._temp_cabin),
            chr(5),
            chr(0),
        )

    def _render_line3(self, t: float) -> str:
        return '{:>2}/{:>3}{}  {:>5}{}{}'.format(
            # Energy
            '--' if self._car_max_gen_power is None else '{:.0f}'.format(min(self._car_max_gen_power, 99)),
            '---' if self._car_max_avail_power is None else '{:.0f}'.format(self._car_max_avail_power),
            chr(4),

            # Temp Outdoor
            '---.-' if self._temp_exterior is None else '{:.1f}'.format(self._temp_exterior),
            chr(5),
            chr(1),
        )
