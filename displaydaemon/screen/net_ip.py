from typing import Optional, List, Dict
import dot3k.joystick as nav

import displaydaemon.d3k as d3k_icons
from displaydaemon.keys import NET_DATA
from displaydaemon.screen.base import BaseScreen


class NetIpScreen(BaseScreen):
    def __init__(self):
        super().__init__(True, True)
        self._eth_ip: Optional[str] = None
        self._usb_ip: Optional[str] = None
        self._wifi_ip: Optional[str] = None

    def setup_chars(self) -> Dict[int, List[int]]:
        return {
            1: d3k_icons.ICON_GPS_IMG,
            2: d3k_icons.ICON_NET_IMG,
            3: d3k_icons.ICON_CAR_IMG2,
            4: d3k_icons.ICON_CELLULAR,
            5: d3k_icons.ICON_WIFI
        }

    def handle_input(self, input: int) -> Optional[str]:
        return 'clock' if input == nav.LEFT else None

    def update_data(self, t: float, data: dict):
        self._eth_ip = data.get(NET_DATA['if_eth'], None)
        self._usb_ip = data.get(NET_DATA['if_usb'], None)
        self._wifi_ip = data.get(NET_DATA['if_wifi'], None)

    def render(self, t: float) -> List[str]:
        return [
            '{}{}'.format(chr(2), self._eth_ip or '---'),
            '{}{}'.format(chr(4), self._usb_ip or '---'),
            '{}{}'.format(chr(5), self._wifi_ip or '---'),
        ]


