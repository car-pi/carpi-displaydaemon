from typing import List, Optional, Dict, Tuple


class BaseScreen(object):
    def __init__(self,
                 use_custom_chars: bool,
                 use_data: bool):
        self._use_custom_chars = use_custom_chars
        self._use_data = use_data

    @property
    def use_custom_chars(self) -> bool:
        return self._use_custom_chars

    @property
    def use_data(self) -> bool:
        return self._use_data

    def setup_chars(self) -> Dict[int, List[int]]:
        return {}

    def handle_input(self, input: int) -> Optional[str]:
        return None

    def update_data(self, t: float, data: dict):
        return None

    def render(self, t: float) -> List[str]:
        raise NotImplementedError()

    def render_backlight(self, t: float, use_dothat: bool) -> Optional[List[Tuple[int, int, int]]]:
        return None


class EmptyScreen(BaseScreen):
    def __init__(self):
        super().__init__(False, False)

    def render(self, _: float) -> List[str]:
        return ['', '', '']
