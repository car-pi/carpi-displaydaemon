from displaydaemon.screen.base import EmptyScreen
from displaydaemon.screen.car import CarScreen
from displaydaemon.screen.clock import ClockScreen
from displaydaemon.screen.net_ip import NetIpScreen

KNOWN_SCREENS = {
    'empty': EmptyScreen(),
    'clock': ClockScreen(),
    'net_ips': NetIpScreen(),
    'car': CarScreen()
}
