from datetime import datetime
from math import floor
from typing import Optional, List, Dict, Tuple

import isodate

from displaydaemon.input import KEY_RIGHT
from displaydaemon.keys import GPS_DATA, NET_DATA, OBD_DATA, LOG_DATA
from displaydaemon.screen.base import BaseScreen
from displaydaemon.utils import flashing_indicator, DOT3K_CHAR_WIDTH, show_flashing, has_bit_set
import displaydaemon.d3k as d3k_icons


MIN_GPS_ACC = float(1)
MAX_GPS_ACC = float(150)


GPS_STATE_OFF = 0x00
GPS_STATE_ON = 0x10
GPS_STATE_SEARCHING = 0x11

NET_STATE_OFF = 0x00
NET_STATE_OFFLINE = 0x01
NET_STATE_CONNECTED = 0x11

CAR_STATE_OFF = 0x00
CAR_STATE_OFFLINE = 0x01
CAR_STATE_CONNECTED = 0x11

LOG_STATE_OFF = 0b00
LOG_STATE_ALIVE = 0b01
LOG_STATE_ALIVE_BIT = 0
LOG_STATE_LOGGING = 0b10
LOG_STATE_LOGGING_BIT = 1


def ip_has_value(s: Optional[str]) -> bool:
    return s is not None and not s == 'N/A'


class ClockScreen(BaseScreen):
    def __init__(self):
        super().__init__(True, True)
        self._gps_state: int = GPS_STATE_OFF
        self._net_state: int = NET_STATE_OFF
        self._car_state: int = CAR_STATE_OFF
        self._eth_ip: Optional[str] = None
        self._usb_ip: Optional[str] = None
        self._wifi_ip: Optional[str] = None
        self._obd_voltage: Optional[float] = None
        self._current_center_line: int = 0
        self._car_soc: Optional[float] = None
        self._car_avail_energy: Optional[float] = None
        self._log_state: int = LOG_STATE_OFF

    def setup_chars(self) -> Dict[int, List[int]]:
        return {
            1: d3k_icons.ICON_GPS_IMG,
            2: d3k_icons.ICON_NET_IMG,
            3: d3k_icons.ICON_CAR_IMG2,
            4: d3k_icons.ICON_CELLULAR,
            5: d3k_icons.ICON_WIFI
        }

    def render_backlight(self, t: float, use_dothat: bool) -> Optional[List[Tuple[int, int, int]]]:
        return [(150, 0, 0)]

    def update_data(self,
                    t: float,
                    data: dict):
        self._gps_state = self._update_gps_state(
            t,
            data.get(GPS_DATA['systimestamp'], None),
            max(data.get(GPS_DATA['epx'], 9999), data.get(GPS_DATA['epy'], 9999)))
        self._net_state = self._update_net_state(
            t,
            data.get(NET_DATA['systimestamp'], None),
            data.get(NET_DATA['expect_next_ts'], None),
            data.get(NET_DATA['is_online'], False))
        self._car_state = self._update_car_state(
            t,
            data.get(OBD_DATA['systimestamp'], None))

        self._obd_voltage = data.get(OBD_DATA['voltage'], None)
        self._car_soc = data.get(OBD_DATA['bat_soc'], None)
        self._car_avail_energy = data.get(OBD_DATA['bat_available_energy'], None)

        self._eth_ip = data.get(NET_DATA['if_eth'], None)
        self._usb_ip = data.get(NET_DATA['if_usb'], None)
        self._wifi_ip = data.get(NET_DATA['if_wifi'], None)

        self._log_state = self._update_log_state(t,
                                                 data.get(LOG_DATA['systimestamp']),
                                                 data.get(LOG_DATA['is_logging']))

        self._current_center_line = 0

    def _update_log_state(self,
                          t: float,
                          sys_ts: Optional[str],
                          is_logging: Optional[bool]) -> int:
        if sys_ts is None:
            return LOG_STATE_OFF
        try:
            ts = isodate.parse_datetime(sys_ts).timestamp()
        except:
            return LOG_STATE_OFF

        if t - ts > 8:  # Heartbeat of DataLoggerD is 5s so this should catch any failure
            return LOG_STATE_OFF

        state = LOG_STATE_ALIVE
        if is_logging:
            state = state | LOG_STATE_LOGGING
        return state

    def _update_gps_state(self,
                          t: float,
                          sys_ts: Optional[str],
                          gps_acc: float) -> int:
        if sys_ts is None:
            return GPS_STATE_OFF
        ts = datetime.strptime(sys_ts, '%Y-%m-%d %H:%M:%S.%f').timestamp()
        if t - ts < 10:
            return GPS_STATE_ON \
                if MIN_GPS_ACC < gps_acc < MAX_GPS_ACC else \
                GPS_STATE_SEARCHING
        return GPS_STATE_OFF

    def _update_net_state(self,
                          t: float,
                          sys_ts: Optional[str],
                          expect_next_ts: Optional[str],
                          is_online: bool) -> int:
        if sys_ts is None:
            return NET_STATE_OFF

        try:
            ts = isodate.parse_datetime(sys_ts).timestamp()
            expect_next = isodate.parse_datetime(expect_next_ts).timestamp() \
                if expect_next_ts is not None \
                else 0
        except:
            return NET_STATE_OFF

        if t - ts > 10:
            return NET_STATE_OFF
        if t < expect_next:
            return NET_STATE_CONNECTED if is_online else NET_STATE_OFFLINE
        else:
            return NET_STATE_OFFLINE

    def _update_car_state(self,
                          t: float,
                          sys_ts: Optional[str]) -> int:
        if not sys_ts:
            return CAR_STATE_OFF

        try:
            ts = isodate.parse_datetime(sys_ts).timestamp()
        except:
            return NET_STATE_OFF

        return CAR_STATE_OFFLINE if t > ts + 5 else CAR_STATE_CONNECTED

    def handle_input(self, input: int) -> Optional[str]:
        if input == KEY_RIGHT:
            return 'car'
        return None

    def render(self, t: float) -> List[str]:
        return [
            datetime.now().strftime(flashing_indicator('%d/%m   %H:%M:%S', '%d/%m   %H %M %S', t)),
            self._render_center_line(t),
            self._build_status_line(t).center(DOT3K_CHAR_WIDTH)
        ]

    def _render_center_line(self, t: float) -> str:
        options = [
            'CarPi'.center(DOT3K_CHAR_WIDTH)
        ]
        if ip_has_value(self._usb_ip):
            options.append('{}{}'.format(chr(4), self._usb_ip.rjust(DOT3K_CHAR_WIDTH - 1)))
        if ip_has_value(self._eth_ip):
            options.append('{}{}'.format(chr(2), self._eth_ip.rjust(DOT3K_CHAR_WIDTH - 1)))
        if ip_has_value(self._wifi_ip):
            options.append('{}{}'.format(chr(5), self._wifi_ip.rjust(DOT3K_CHAR_WIDTH - 1)))

        if not ip_has_value(self._usb_ip) and not ip_has_value(self._eth_ip):
            options.append('{} No Network'.format(chr(2)))

        if self._car_state == CAR_STATE_CONNECTED:
            soc_opt = '{}SoC: {:>10}'.format(
                chr(3),
                '{:0.2f}%'.format(self._car_soc) if self._car_soc is not None
                else '-.--%'
            )
            options.append(soc_opt)

            energy_opt = '{}ENGY: {:>6}kWh'.format(
                chr(3),
                '{:0.2f}'.format(self._car_avail_energy) if self._car_avail_energy is not None
                else '-.--'
            )
            options.append(energy_opt)

        current_pos = floor(t / 5) % len(options)
        return options[current_pos]

    def _build_status_line(self, t: float) -> str:
        return str.join(' ', [
            self._get_gps_indicator(t),
            self._get_net_indicator(t),
            self._get_car_indicator(t),
            self._get_logger_indicator(t)
        ])

    def _get_gps_indicator(self, t: float) -> str:
        icon = chr(1)
        if self._gps_state == GPS_STATE_OFF:
            icon = flashing_indicator(icon, ' ', t)
        elif self._gps_state == GPS_STATE_SEARCHING:
            icon = flashing_indicator('[{} '.format(icon), ' {}]'.format(icon), t)
        elif self._gps_state == GPS_STATE_ON:
            icon = '[{}]'.format(icon)

        return icon.center(3)

    def _get_net_indicator(self, t: float) -> str:
        icon = chr(2)
        if self._net_state == NET_STATE_OFF:
            icon = flashing_indicator(icon, ' ', t)
        elif self._net_state == NET_STATE_OFFLINE:
            icon = flashing_indicator('[{}]'.format(icon),
                                      icon,
                                      t)
        elif self._net_state == NET_STATE_CONNECTED:
            icon = '[{}]'.format(icon)

        return icon.center(3)

    def _get_car_indicator(self, t: float) -> str:
        icon = chr(3)
        if self._car_state == CAR_STATE_OFF:
            icon = flashing_indicator(icon, ' ', t)
        elif self._car_state == CAR_STATE_OFFLINE:
            icon = flashing_indicator('[{}]'.format(icon),
                                      icon,
                                      t)
        elif self._car_state == CAR_STATE_CONNECTED:
            icon = '[{}]'.format(icon)

        return icon.center(3)

    def _get_logger_indicator(self, t: float) -> str:
        if self._log_state == LOG_STATE_OFF:
            return ' _ '
        if self._log_state == LOG_STATE_ALIVE | LOG_STATE_LOGGING:
            return flashing_indicator('>L ', ' L>', t)
        return 'L'.center(3)
