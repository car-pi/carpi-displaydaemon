#  CARPI DISPLAY DAEMON
#  (C) 2021, Raphael "rGunti" Guntersweiler
#  Licensed under MIT
from daemoncommons.daemon import DaemonRunner

from displaydaemon.daemon import DisplayDaemon

import dot3k.joystick as joy_nav
import dothat.touch as touch_nav


DAEMON = DisplayDaemon()


# This method has to reside here because we can't attach it
# to an object instance
@joy_nav.on([
    joy_nav.UP,
    joy_nav.DOWN,
    joy_nav.LEFT,
    joy_nav.RIGHT,
    joy_nav.BUTTON
])
def handle_dot3k_input(p):
    DAEMON.handle_input(p)


@touch_nav.on([
    touch_nav.UP,
    touch_nav.DOWN,
    touch_nav.LEFT,
    touch_nav.RIGHT,
    touch_nav.BUTTON,
    touch_nav.CANCEL
])
def handle_dothat_input(p):
    DAEMON.handle_input(p.channel)


if __name__ == '__main__':
    d = DaemonRunner('DISPLAYD_CFG', ['displayd.ini', '/etc/carpi/displayd.ini'])
    d.run(DAEMON)
