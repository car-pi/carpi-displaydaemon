#  CARPI DISPLAY DAEMON
#  (C) 2022, Raphael "rGunti" Guntersweiler
#  Licensed under MIT

from dot3k import joystick
from dothat import touch


KEY_UP = 1
KEY_DOWN = 2
KEY_LEFT = 4
KEY_RIGHT = 8
KEY_ENTER = 16
KEY_BACK = 32


DOT3K_MAPPING = {
    joystick.UP: KEY_UP,
    joystick.DOWN: KEY_DOWN,
    joystick.LEFT: KEY_LEFT,
    joystick.RIGHT: KEY_RIGHT,
    joystick.BUTTON: KEY_ENTER
}
DOTHAT_MAPPING = {
    touch.UP: KEY_UP,
    touch.DOWN: KEY_DOWN,
    touch.LEFT: KEY_LEFT,
    touch.RIGHT: KEY_RIGHT,
    touch.BUTTON: KEY_ENTER,
    touch.CANCEL: KEY_BACK
}


KEY_STR = {
    KEY_UP: 'UP',
    KEY_DOWN: 'DOWN',
    KEY_LEFT: 'LEFT',
    KEY_RIGHT: 'RIGHT',
    KEY_ENTER: 'ENTER',
    KEY_BACK: 'BACk'
}


def parse_dot3k(key: int) -> int:
    return DOT3K_MAPPING[key]


def parse_dothat(key: int) -> int:
    return DOTHAT_MAPPING[key]
