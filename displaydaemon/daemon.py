#  CARPI DISPLAY DAEMON
#  (C) 2021, Raphael "rGunti" Guntersweiler
#  Licensed under MIT

import time
from logging import Logger
from time import sleep
from typing import Optional, Tuple, Any

from carpicommons.log import logger
from daemoncommons.daemon import Daemon
from redisdatabus.bus import TypedBusListener

from displaydaemon.input import parse_dothat, parse_dot3k, KEY_STR
from displaydaemon.keys import INPUT_DATA_KEYS
from displaydaemon.screen.base import BaseScreen, EmptyScreen
from displaydaemon.screen.registry import KNOWN_SCREENS
from displaydaemon.utils import fit_string, DOT3K_CHAR_WIDTH

FRAME_DEBUG_TICKER = 10


class DisplayDaemon(Daemon):
    def __init__(self):
        super().__init__('DisplayDaemon')
        self._is_running = True
        self._logger: Optional[Logger] = None
        self._ftick = 0
        self._bus: Optional[TypedBusListener] = None
        self._current_values = dict()
        self._show_custom_chars = True
        self._use_dothat = False

        self._lcd = None
        self._backlight = None

        self._current_screen: BaseScreen = EmptyScreen()
        self._next_screen: str = None

    def shutdown(self):
        self._logger.info('Shutting down daemon ...')
        self._is_running = False
        if self._bus:
            self._bus.stop()

    def startup(self):
        self._logger = log = logger(self.name)
        log.info('Starting up daemon ...')

        log.info('Reading Settings ...')
        init_color_a = self._get_config('Display', 'DefaultColor', '255,255,255').split(',')
        init_color = (int(init_color_a[0]), int(init_color_a[1]), int(init_color_a[2]))
        contrast = self._get_config_int('Display', 'Contrast', 50)
        enable_rgb_flip = self._get_config_bool('Display', 'EnableRgbFlip', False)
        self._use_dothat = use_dothat = self._get_config_bool('Display', 'DotHat', False)

        log.info('Initializing Display ...')
        self._init_display(init_color, contrast, enable_rgb_flip, use_dothat)

        log.info('Initializing Frame Limiter ...')
        fps_limit = self._get_config_float('Display', 'FpsLimit', 10)
        target_frame_time = 0
        if fps_limit > 0:
            target_frame_time = float(1) / fps_limit
            log.info('Frame Limiter set to %.1f FPS (max frame time of %.3f s)', fps_limit, target_frame_time)
        else:
            log.warning('Frame Limiter disabled, will use all available CPU resources for rendering '
                        '(Disable by setting Display.FpsLimit in displayd.ini)')

        self._bus = bus = self._build_bus_reader(INPUT_DATA_KEYS)
        bus.register_global_callback(self._on_new_value_registered)

        log.info('Starting Bus Reader ...')
        bus.start()

        # Keep the Boot Display 1 second
        sleep(1)

        self._lcd.clear()

        # Set main screen
        start_screen = self._get_config('Display', 'StartScreen', 'clock')
        self._set_current_screen(start_screen)

        while self._is_running:
            started = time.time()
            self._render()
            self._limit_fps(target_frame_time, started)

    def _build_bus_reader(self, channels: list) -> TypedBusListener:
        self._logger.info('Connecting to Data Source Redis instance with %s keys ...',
                          len(channels))
        return TypedBusListener(channels,
                                host=self._get_config('Redis', 'Host', '127.0.0.1'),
                                port=self._get_config_int('Redis', 'Port', 6379),
                                db=self._get_config_int('Redis', 'DB', 0),
                                password=self._get_config('Redis', 'Password', None))

    def _on_new_value_registered(self, channel: str, value: Any):
        self._logger.debug('Received new value from %s: %s', channel, value)
        self._current_values[channel] = value

    def _limit_fps(self, target_time: float, started_at: float):
        if target_time <= 0:
            return

        now = time.time()
        time_taken = now - started_at
        if time_taken > target_time:
            self._logger.warning('Frame time breached! %.3f expected, %.3f taken',
                                 target_time, time_taken)
            return

        if self._ftick + FRAME_DEBUG_TICKER < now:
            self._ftick = now
            self._logger.info('Frame time %.3f s, expected %.3f s', time_taken, target_time)

        sleep(target_time - time_taken)

    def _init_display(self,
                      init_color: Tuple[int, int, int],
                      contrast: int,
                      enable_rgb_flip: bool = False,
                      use_dothat: bool = False):
        if use_dothat:
            import dothat.backlight as backlight
            import dothat.lcd as lcd
        else:
            import dot3k.backlight as backlight
            import dot3k.lcd as lcd

        self._lcd = lcd
        self._backlight = backlight

        backlight.off()
        sleep(0.5)

        lcd.clear()
        lcd.set_contrast(contrast)
        lcd.set_cursor_position(0, 0)
        lcd.write('CARPI DISPLAYD'.center(DOT3K_CHAR_WIDTH))
        lcd.set_cursor_position(0, 2)
        lcd.write('Starting ...'.center(DOT3K_CHAR_WIDTH))

        if enable_rgb_flip:
            backlight.use_rbg()

        backlight.rgb(init_color[0], init_color[1], init_color[2])
        backlight.update()

    def handle_input(self, pin: int):
        if self._use_dothat:
            common_key = parse_dothat(pin)
        else:
            common_key = parse_dot3k(pin)

        if self._logger:
            self._logger.info('Got input: %.0f (%s)', common_key, KEY_STR[common_key])
        next_screen = self._current_screen.handle_input(common_key)
        if next_screen:
            self._next_screen = next_screen

    def _set_current_screen(self, screen: str):
        self._logger.debug('Changing to new screen to %s ...', screen)
        screen_inst = KNOWN_SCREENS[screen]
        self._current_screen = screen_inst
        if screen_inst.use_custom_chars:
            self._logger.debug('Setting up new characters ...')
            chars = screen_inst.setup_chars()
            for k, v in chars.items():
                self._lcd.create_char(k, v)

    def _render(self):
        if self._next_screen is not None:
            self._set_current_screen(self._next_screen)
            self._next_screen = None

        screen = self._current_screen

        t = time.time()
        if screen.use_data:
            screen.update_data(t, self._current_values)
        lines = list(map(lambda s: fit_string(s, DOT3K_CHAR_WIDTH), screen.render(t)))
        colors = screen.render_backlight(t, self._use_dothat)

        self._lcd.set_cursor_offset(0)
        self._lcd.write(str.join('', lines))

        if colors is not None:
            if len(colors) == 1:
                self._backlight.rgb(colors[0][0],
                                    colors[0][1],
                                    colors[0][2])
            else:
                for idx in range(len(colors)):
                    color = colors[idx]
                    if color is not None:
                        self._backlight.single_rgb(idx,
                                                   color[0],
                                                   color[1],
                                                   color[2],
                                                   False)
            self._backlight.update()
