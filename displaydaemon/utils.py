#  CARPI DISPLAY DAEMON
#  (C) 2021, Raphael "rGunti" Guntersweiler
#  Licensed under MIT

DOT3K_CHAR_WIDTH = 16


def fit_string(s: str, length: int) -> str:
    j = s.ljust(length)
    return j if len(j) <= length else j[0:length]


def show_flashing(t: float, limit: float = 0.5, div: float = 1) -> bool:
    return (t / div) % 1 < limit


def flashing_indicator(on: str, off: str, t: float, limit: float = 0.5, div: float = 1) -> str:
    return on if show_flashing(t, limit, div) else off


def has_bit_set(n: int, bit: int) -> bool:
    return n & pow(2, bit) >> bit
