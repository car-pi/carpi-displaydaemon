#  CARPI DISPLAY DAEMON
#  (C) 2021, Raphael "rGunti" Guntersweiler
#  Licensed under MIT

from redisdatabus.bus import TypedBusListener

import gpsdaemon.keys as gpskeys
import netdaemon.keys as netkeys
import obddaemon.canbus.keys as obdkeys


def build_foreign_key(type: str, domain: str, key: str) -> str:
    return '{}{}{}'.format(type, domain, key)


GPS_DATA = {
    'latitude': gpskeys.KEY_LATITUDE,
    'longitude': gpskeys.KEY_LONGITUDE,
    'altitude': gpskeys.KEY_ALTITUDE,
    'epx': gpskeys.KEY_EPX,
    'epy': gpskeys.KEY_EPY,
    'speed': gpskeys.KEY_SPEED,
    'heading': gpskeys.KEY_TRACK,
    'timestamp': gpskeys.KEY_TIMESTAMP,
    'systimestamp': gpskeys.KEY_SYS_TIMESTAMP
}
NET_DATA = {
    'is_online': netkeys.KEY_IS_ONLINE,
    'systimestamp': netkeys.KEY_SYS_TIMESTAMP,
    'expect_next_ts': netkeys.KEY_PING_NEXT_TIMESTAMP,
    'if_eth': netkeys.KEY_IP_ETH,
    'if_usb': netkeys.KEY_IP_USB,
    'if_wifi': netkeys.KEY_IP_WIFI
}
OBD_DATA = {
    'systimestamp': obdkeys.KEY_SYS_TIMESTAMP,
    'systimestamp_sim': '{}.orig'.format(obdkeys.KEY_SYS_TIMESTAMP),
    'voltage': obdkeys.KEY_OBD_VOLTAGE,
    'bat_soc': build_foreign_key(TypedBusListener.TYPE_PREFIX_FLOAT, obdkeys.KEY_BASE, 'BatterySoC'),
    'bat_available_energy': build_foreign_key(TypedBusListener.TYPE_PREFIX_FLOAT,
                                              obdkeys.KEY_BASE,
                                              'BatteryAvailableEnergy'),
    'bat_voltage': build_foreign_key(TypedBusListener.TYPE_PREFIX_FLOAT, obdkeys.KEY_BASE, 'BatteryVoltage'),
    'bat_current': build_foreign_key(TypedBusListener.TYPE_PREFIX_FLOAT, obdkeys.KEY_BASE, 'BatteryCurrent'),
    'speed': build_foreign_key(TypedBusListener.TYPE_PREFIX_FLOAT, obdkeys.KEY_BASE, 'VehicleSpeed'),
    'state': build_foreign_key(TypedBusListener.TYPE_PREFIX_STRING, obdkeys.KEY_BASE, 'VehicleState.parsed'),
    'eco_mode': build_foreign_key(TypedBusListener.TYPE_PREFIX_FLOAT, obdkeys.KEY_BASE, 'EcoMode'),
    'temp_bat': build_foreign_key(TypedBusListener.TYPE_PREFIX_FLOAT, obdkeys.KEY_BASE, 'AvgBatteryTemp'),
    'temp_in': build_foreign_key(TypedBusListener.TYPE_PREFIX_FLOAT, obdkeys.KEY_BASE, 'InCarTemperature'),
    'temp_out': build_foreign_key(TypedBusListener.TYPE_PREFIX_FLOAT, obdkeys.KEY_BASE, 'ExternalTemperature'),
    'max_charge': build_foreign_key(TypedBusListener.TYPE_PREFIX_FLOAT, obdkeys.KEY_BASE, 'MaxGeneratedPower'),
    'max_power': build_foreign_key(TypedBusListener.TYPE_PREFIX_FLOAT, obdkeys.KEY_BASE, 'MaxAvailablePower')
}
LOG_DATA = {
    'systimestamp': 's#carpi.logd.ts',
    'is_logging': 'b#carpi.logd.is_logging'
}

INPUT_DATA_KEYS = list(set(GPS_DATA.values())) \
                  + list(set(NET_DATA.values())) \
                  + list(set(OBD_DATA.values())) \
                  + list(set(LOG_DATA.values()))
